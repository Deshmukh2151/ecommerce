import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
  import { from, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
    private _loginUrl=`http://35.154.208.218:5000/v1/customer/signIn`;
    
  constructor(private http:HttpClient) { }
  login(data):Observable<any>{
    console.log("dataoflogin", data);
    return this.http.post(this._loginUrl, data);

  }
}
