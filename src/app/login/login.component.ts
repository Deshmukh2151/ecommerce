import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthServiceService } from '../auth-service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formGroup:FormGroup;

  constructor(private authService:AuthServiceService) { }

  ngOnInit(): void {
    this.initForm();
  }

  // To set form value
  initForm(){
    this.formGroup= new FormGroup({
      username:new FormControl('',[Validators.required]),
      password:new FormControl('',[Validators.required])
    });
    console.log("this.formGroup",this.formGroup)
  }
  loginProcess(formGroup){
    // alert('In login')
    console.log("this.formGroup99999",formGroup.value)
    
    if(this.formGroup.valid){
      this.authService.login(this.formGroup.value).subscribe(result=>{
      
           console.log(result);
          //  alert(result);
          localStorage.setItem('token',result.token)
            });
    }
  }
}
